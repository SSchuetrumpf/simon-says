package edu.apsu.csci.stephenschuetrumpf.simonsays;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener
{

    private final int NORMAL_GAME = 0;
    private final int HARD_GAME = 1;
    private final int EXPERT_GAME = 2;
    private SharedPreferences prefs;
    private Button normal;
    private Button hard;
    private Button expert;
    
    private MediaPlayer mPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prefs = getSharedPreferences("high_scores", Context.MODE_PRIVATE);

        normal = (Button) findViewById(R.id.normal_mode_button);
        hard = (Button) findViewById(R.id.hard_mode_button);
        expert = (Button) findViewById(R.id.expert_mode_button);

        normal.setOnClickListener(this);
        hard.setOnClickListener(this);
        expert.setOnClickListener(this);

        normal.append(" " + prefs.getInt("normal_high", 0));
        hard.append(" " + prefs.getInt("hard_high", 0));
        expert.append(" " + prefs.getInt("expert_high", 0));
    }
    
    @Override
    protected void onStart()
    {
        mPlayer = MediaPlayer.create(this, R.raw.bgm);
        mPlayer.setOnCompletionListener(new OnCompletionListener()
        {
            
            @Override
            public void onCompletion(MediaPlayer mp)
            {
                mp.setLooping(true);
                mp.start();  
            }
        });
        super.onStart();
    }
    
    @Override
    protected void onResume()
    {
        if(mPlayer != null && !mPlayer.isPlaying())
            mPlayer.start();
        
        super.onResume();
    }
    
    @Override
    protected void onPause()
    {
        if(mPlayer != null)
            mPlayer.pause();
        
        super.onPause();
    }
    
    @Override
    protected void onStop()
    {
        if(mPlayer != null)
        {
            mPlayer.pause();
            mPlayer.release();
            mPlayer = null;
        }
            
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(item.getItemId() == R.id.reset_scores)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Confirm")
            .setMessage("Are you sure you wish to reset scores?")
            .setPositiveButton("Yes", new DialogInterface.OnClickListener()
            {
                
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    prefs.edit().clear().commit();
                    refreshButtons();
                    dialog.dismiss();
                    
                }
            })
            .setNegativeButton("No", new DialogInterface.OnClickListener()
            {
                
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    dialog.dismiss();
                    
                }
            })
            .show();
            return true;
        }
        else
            return super.onOptionsItemSelected(item);
    }

    protected void refreshButtons()
    {
        normal.setText(R.string.normal_button);
        normal.append(" " + prefs.getInt("normal_high", 0));
        
        hard.setText(R.string.hard_button);
        hard.append(" " + prefs.getInt("hard_high", 0));
        
        expert.setText(R.string.expert_button);
        expert.append(" " + prefs.getInt("expert_high", 0));
        
    }

    public void onClick(View v)
    {
        Intent game = new Intent(this, GameActivity.class);
        switch (v.getId())
        {
            case R.id.normal_mode_button:
                game.putExtra("mode", NORMAL_GAME);
                game.putExtra("high_score", prefs.getInt("normal_high", 0));
                break;
            case R.id.hard_mode_button:
                game.putExtra("mode", HARD_GAME);
                game.putExtra("high_score", prefs.getInt("hard_high", 0));
                break;
            case R.id.expert_mode_button:
                game.putExtra("mode", EXPERT_GAME);
                game.putExtra("high_score", prefs.getInt("expert_high", 0));
                break;
        }

        startActivityForResult(game, game.getIntExtra("mode", NORMAL_GAME));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode == RESULT_OK)
        {
            int highScore = data.getIntExtra("score", 0);
            Log.d("GameActivity", "RETURNED SCORE IS  " + highScore);
            switch (requestCode)
            {
                case NORMAL_GAME:
                    if (data.getBooleanExtra("new_high", false))
                        prefs.edit().putInt("normal_high", highScore).commit();
                    break;

                case HARD_GAME:
                    if (data.getBooleanExtra("new_high", false))
                        prefs.edit().putInt("hard_high", highScore).commit();
                    break;

                case EXPERT_GAME:
                    if (data.getBooleanExtra("new_high", false))
                        prefs.edit().putInt("expert_high", highScore).commit();
                    break;
            }
            
            refreshButtons();
        }
    }

}
