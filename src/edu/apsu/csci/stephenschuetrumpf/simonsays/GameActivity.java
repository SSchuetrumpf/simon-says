package edu.apsu.csci.stephenschuetrumpf.simonsays;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class GameActivity extends Activity implements View.OnClickListener
{
    private SoundPool pool;
    private Button[] buttons;

    private int numberOfRequiredMoves = 0;
    private int moveCount = 0;

    List<Integer> moveList;
    private int[] soundList;
    private int currentPlaying;

    private final long GENERATION_DELAY = 2000L;
    private final long SEQUENCE_DELAY_START_POINT = 790L;
    private final long SEQUENCE_DELAY_MIN = 250L;

    private long sequenceDelay;
    private long sequenceDelayAdditive = sequenceDelay;
    private final double SEQUENCE_DELAY_SCALAR = 0.05;

    private SequenceGenerator sequenceGenerator;
    public boolean isGeneratorInteracting;

    private SensorManager mSensorManager;
    private Sensor mSensor;
    private final SensorEventListener mSensorListener = new SensorEventListener()
    {

        @Override
        public void onSensorChanged(SensorEvent event)
        {
            Log.d("SENSOR_DATA", "====================");
            for (float vals : event.values)
            {
                mSeed += Math.abs(vals);
                Log.d("SENSOR_DATA", "VALUES = " + vals);
            }

            Log.d("SENSOR_DATA", "SEED + " + mSeed);

            Log.d("SENSOR_DATA", "====================");

        }

        // I don't care when the accuracy changes. I don't need accuracy of the
        // sensor,
        // I just need some kind of value to influence the number generator.
        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy)
        {

        }
    };

    // This prevents the variable from being loaded into caches. 
    // When it gets loaded into a thread's cache, that cache may or 
    // may not be updated whenever another thread updates the variable.
    // Preventing this caching prevents "old" data from being used.
    // Aren't threads fun?
    private volatile float mSeed = 0;

    private boolean mButtonsEnabled;

    // Runnable for toggling the state of the buttons. There's
    // probably a better way to do this, but oh well.
    // I used a runnable so I could call runOnUiThread
    // on it from in my AsyncTask.
    private Runnable buttonToggle = new Runnable()
    {
        @Override
        public void run()
        {
            for (Button b : buttons)
                b.setEnabled(mButtonsEnabled);
        }
    };

    public static enum GameMode
    {
        NORMAL, HARD, EXPERT
    }

    private GameMode mGameMode;
    private int mHighScore;
    private boolean newHigh;

    private int mNumLives;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.game_activity);

        switch (getIntent().getIntExtra("mode", 0))
        {
            case 0:
                mGameMode = GameMode.NORMAL;
                break;
            case 1:
                mGameMode = GameMode.HARD;
                break;
            case 2:
                mGameMode = GameMode.EXPERT;
                break;
        }

        mHighScore = getIntent().getIntExtra("high_score", 0);
        Log.d(getClass().getSimpleName(), "HIGH SCORE: " + mHighScore);

        pool = new SoundPool(4, AudioManager.STREAM_MUSIC, 1);
        soundList = new int[4];

        // Make a list to shuffle our sounds if we're in expert mode.
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < 4; i++)
            list.add(i);

        // Shuffle soundList indexes
        if (mGameMode == GameMode.EXPERT)
            Collections.shuffle(list);

        soundList[list.get(0)] = pool.load(this, R.raw.beep_1, 0);
        soundList[list.get(1)] = pool.load(this, R.raw.beep_2, 0);
        soundList[list.get(2)] = pool.load(this, R.raw.beep_3, 0);
        soundList[list.get(3)] = pool.load(this, R.raw.beep_4, 0);
        buttons = new Button[4];
        moveList = new ArrayList<Integer>();

        buttons[0] = (Button) findViewById(R.id.button1);
        buttons[1] = (Button) findViewById(R.id.button2);
        buttons[2] = (Button) findViewById(R.id.button3);
        buttons[3] = (Button) findViewById(R.id.button4);

        // Sets the volume rocker buttons to control the music stream. Sound
        // pool
        // normally takes care of this automatically, but since my sounds
        // are so short, the user doesn't have time to change the volume before
        // it swaps back to ringtone volume. Setting it this way lets the user
        // Adjust the volume at any time.
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        for (View v : buttons)
        {
            v.setEnabled(false);
            v.setOnClickListener(this);

        }

        // Get the sensor manager, and if possible, the accelerometer. Not all
        // devices
        // have an accelerometer, i.e. the emulator, so this is a compatibility
        // check to
        // prevent crashes.
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        if (mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null)
        {
            mSensor = mSensorManager
                    .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        }

        startGame();

    }

    @Override
    protected void onResume()
    {
        // Register our listener to the sensor manager.
        if (mSensorManager != null && mSensor != null)
            mSensorManager.registerListener(mSensorListener, mSensor,
                    SensorManager.SENSOR_DELAY_NORMAL);

        super.onResume();
    }

    private void startGame()
    {
        String extra = "";
        if (mGameMode == GameMode.EXPERT)
            extra = "In expert mode, the sounds have been randomized"
                    + " and the buttons will not flash. Due to the "
                    + "impossibility of this, you have been given "
                    + "two lives, which show at the top.";
        new AlertDialog.Builder(this)
                .setMessage("Press OK to start. " + extra)
                .setTitle("Start")
                .setPositiveButton("OK", new DialogInterface.OnClickListener()
                {

                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                        moveList.clear();
                        moveCount = 0;
                        numberOfRequiredMoves = 0;
                        mButtonsEnabled = false;
                        sequenceDelay = SEQUENCE_DELAY_START_POINT;
                        sequenceDelayAdditive = SEQUENCE_DELAY_START_POINT;
                        buttonToggle.run();

                        if (mGameMode == GameMode.EXPERT)
                        {
                            setLives(2);
                            findViewById(R.id.textView1).setVisibility(
                                    View.VISIBLE);
                            findViewById(R.id.livesTV).setVisibility(
                                    View.VISIBLE);
                        }
                        else
                        {
                            setLives(1);
                            findViewById(R.id.textView1).setVisibility(
                                    View.INVISIBLE);
                            findViewById(R.id.livesTV).setVisibility(
                                    View.INVISIBLE);
                        }
                        // mDemo = false;

                        if (sequenceGenerator == null)
                            sequenceGenerator = new SequenceGenerator();

                        sequenceGenerator.execute();

                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener()
                        {

                            @Override
                            public void onClick(DialogInterface dialog,
                                    int which)
                            {
                                dialog.dismiss();
                                // mDemo = true;

                            }
                        }).show();

    }

    // If the generator and/or sound pool happen to be playing, stop them.
    // Also, unregister our listener from the sensor manager. If we leave
    // it registered, it will keep receiving data needlessly.
    // Pretty sure it also drains the battery if it's still there.
    // Haven't tested the battery part.
    @Override
    protected void onPause()
    {
        pool.stop(currentPlaying);
        if (sequenceGenerator != null)
            sequenceGenerator.cancel(true);

        sequenceGenerator = null;
        mSensorManager.unregisterListener(mSensorListener);
        super.onPause();
    }

    // Adds a menu to the activity. This menu simply has a
    // 'Reset Game' button that zeros everything out.
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == R.id.reset_game)
        {
            startGame();
            return true;
        }
        else
            return super.onOptionsItemSelected(item);
    }

    // If the back button is pressed, end the game, and cancel the result.
    // No need to attempt to save a score if the game was ended
    // prematurely.
    @Override
    public void onBackPressed()
    {
        setResult(RESULT_CANCELED);
        finish();
        // super.onBackPressed();
    }

    @Override
    public void onClick(View v)
    {
        int index = 0;

        // Search for the index of the button pressed
        for (int i = 0; i < buttons.length; i++)
            if (v == buttons[i])
            {
                index = i;
                break;
            }

        // Play the sound at that index
        currentPlaying = pool.play(soundList[index], 1f, 1f, 0, 0, 1f);

        // isGeneratorInteracting is true iff the AsyncTask is running.
        // If the generator isn't interating and our index does not match the
        // move
        // Decrement our lives (if expert) and end the game (if needed)
        if (!isGeneratorInteracting && index != moveList.get(moveCount))
        {
            String extra = "";
            newHigh = false;

            setLives(mNumLives - 1);

            if (mNumLives > 0)
                return;

            if ((moveList.size() - 1) > mHighScore)
            {
                newHigh = true;
                extra = "You made a new high score. You completed "
                        + (moveList.size() - 1) + " rounds.";
            }

            new AlertDialog.Builder(this)
                    .setMessage("You failed to complete the round. " + extra)
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener()
                            {

                                @Override
                                public void onClick(DialogInterface dialog,
                                        int which)
                                {
                                    dialog.dismiss();
                                    Intent i = new Intent();
                                    i.putExtra("new_high", newHigh);
                                    i.putExtra("high_score", mHighScore);
                                    i.putExtra("score", (moveList.size() - 1));

                                    moveList.clear();
                                    moveCount = 0;
                                    numberOfRequiredMoves = 0;
                                    mButtonsEnabled = false;
                                    buttonToggle.run();

                                    setResult(RESULT_OK, i);
                                    finish();

                                }
                            }).show();
            return;
        }

        // If the generator isn't interacting, increment our moveCount for the
        // enxt move
        if (!isGeneratorInteracting)
            moveCount++;

        // If our moveCount is equal to the number of required moves,
        // we've completed the sequence correctly.
        if (moveCount == numberOfRequiredMoves && !isGeneratorInteracting)
        {
            moveCount = 0;

            if (sequenceGenerator == null)
                sequenceGenerator = new SequenceGenerator();

            sequenceGenerator.execute();
        }

    }

    // Simply set the new number of lives and set our TextView.
    // This TextView is only shown if in expert mode.
    private void setLives(int num)
    {
        mNumLives = num;

        ((TextView) findViewById(R.id.livesTV)).setText(mNumLives + "");
    }

    private class SequenceGenerator extends AsyncTask<Void, Integer, Void>
    {
        Random numberGenerator;

        public SequenceGenerator()
        {
            numberGenerator = new Random();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            // Disable all buttons to prevent user from clicking.
            mButtonsEnabled = false;
            runOnUiThread(buttonToggle);

            try
            {
                // Wait for two seconds.
                Thread.sleep(GENERATION_DELAY);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }

            // Synchronized with the sensor listener, fetch our seed.
            numberGenerator.setSeed((long) mSeed);

            // Add a new number to our moveList
            moveList.add(numberGenerator.nextInt(4));

            // Increment our required number of moves. I realize it would have
            // worked just as well
            // to use moveList.size(), but this helps prevent several repeated
            // method calls
            numberOfRequiredMoves++;

            // This is where we scale our sequence delay. The sequence only
            // scales
            // as long as it is more than 250ms. If it's more than that, we're
            // going to
            // multiply an additive by 5%, add the 5% to the additive, and
            // subtract it
            // from our delay. It's done this way so that every round, the delay
            // scales faster
            // and faster, instead of the slower and slower you get from
            // multiplying 5%
            // by a smaller and smaller number.
            if (sequenceDelay > SEQUENCE_DELAY_MIN)
            {
                double delay = sequenceDelayAdditive * SEQUENCE_DELAY_SCALAR;
                sequenceDelayAdditive += delay;
                sequenceDelay -= delay;
            }

            // If the sequence delay happens to end up smaller than our minimum,
            // just set it to the minimum.
            if (sequenceDelay < SEQUENCE_DELAY_MIN)
                sequenceDelay = SEQUENCE_DELAY_MIN;

            // Begin displaying our sequence.
            isGeneratorInteracting = true;
            for (int i : moveList)
            {
                try
                {
                    Log.i("GameActivity", "Move " + i);
                    Thread.sleep(sequenceDelay);
                    publishProgress(i, 1);

                    Thread.sleep(sequenceDelay);
                    publishProgress(i, 0);

                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
            isGeneratorInteracting = false;
            sequenceGenerator = null;
            mButtonsEnabled = true;
            runOnUiThread(buttonToggle);
            // The sequence has finished, so re-enable the buttons
            // set the task variable to null, and set
            // isGeneratorInteracting to false
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values)
        {
            int index = values[0];
            int status = values[1];

            if (status == 1)
            {
                // Buttons only flash in normal mode
                // setPressed is a visual change only.
                if (mGameMode == GameMode.NORMAL)
                    buttons[index].setPressed(true);

                // Our code for playing the sounds has already been
                // written in the onClick, so just call the existing
                // method.
                buttons[index].performClick();
            }
            else
            {
                // No need to perform the game mode check because
                // this sets the button state to unpressed. If
                // the button isn't pressed anyway, unpressing
                // it will create no visbile change.
                buttons[index].setPressed(false);
            }
        }
    }

}
